/*!
 * file: BinaryTree.h
 * BinaryTree class definition
 */
#pragma once

#include <string.h>
#include <stdio.h>
#include "CTreeNode.h"

class CBinTree {
public:
    //constructors destructor
    CBinTree();

    CBinTree(int);

    ~CBinTree();

    //methods
    CBinTree readTree(char *);

    void addNode(CTreeNode *&, CTreeNode *&);

    CTreeNode *stringSep(char *);

    void printTree(CTreeNode *, int &);

    void searchNode(CTreeNode *, CTreeNode *&, int);

    void newTree(CTreeNode *&, CTreeNode *, CTreeNode *);

    void writeTree(CTreeNode *, char *, bool);

    void searchAuthor(CTreeNode *, char *, int &);

    void searchTitle(CTreeNode *, char *, int &);

    void deleteTree(CTreeNode *&);

    //attributes
    CTreeNode *root;
    CTreeNode *newroot;

};


#include <iostream>
#include <fstream>
#include "CBinTree.h"

CBinTree::CBinTree() {}
CBinTree::~CBinTree() {}

CBinTree CBinTree::readTree(char *file) {
    std::ifstream fb(file);
    char tmp[127];
    CTreeNode *node;
    root = NULL;

    while (!fb.eof()) {
        fb.getline(tmp, 127);
        if (strlen(tmp) > 1) {
            node = stringSep(tmp);
            addNode(root, node);
        }

    }

    fb.close();
}

void CBinTree::addNode(CTreeNode *&root, CTreeNode *&node) {
    if (root == NULL) {
        root = node;
        root->l = NULL;
        root->r = NULL;
    } else {
        if (node->m_Bookid < root->m_Bookid) {
            if (root->l != NULL) {
                addNode(root->l, node);
            } else {
                root->l = node;
                node->l = NULL;
                node->r = NULL;
            }
        } else if (node->m_Bookid >= root->m_Bookid) {
            if (root->r != NULL) {
                addNode(root->r, node);
            } else {
                root->r = node;
                node->l = NULL;
                node->r = NULL;
            }
        }
    }

}

CTreeNode *CBinTree::stringSep(char *str) {

    char *fstr, tmpstr[127], dbstr[5][127], author[127], title[127];
    int n, i, year, quantity, id;

    strcpy(tmpstr, str);
    fstr = strstr(tmpstr, ";");
    n = fstr - tmpstr;
    strncpy(dbstr[0], &tmpstr[0], n);
    dbstr[0][n] = 0;
    strncpy(fstr, &fstr[1], strlen(fstr));

    for (i = 1; i < 5; i++) {

        strcpy(tmpstr, fstr);
        fstr = strstr(tmpstr, ";");
        if (fstr) {
            n = fstr - tmpstr;
            strncpy(dbstr[i], &tmpstr[0], n);
            dbstr[i][n] = 0;

            strncpy(fstr, &fstr[1], strlen(fstr));
        } else {
            strcpy(dbstr[i], tmpstr);
        }
    }
    id = atoi(dbstr[0]);
    strcpy(author, dbstr[1]);
    strcpy(title, dbstr[2]);
    year = atoi(dbstr[3]);
    quantity = atoi(dbstr[4]);
    CTreeNode *node = new CTreeNode(id, author, title, year, quantity);
    return node;
}


void CBinTree::printTree(CTreeNode *node, int &n) {
    if (node != NULL) {
        n++;
        node->printNode();
        if (node->l != NULL) {
            printTree(node->l, n);
        }
        if (node->r != NULL) {
            printTree(node->r, n);
        }
    }

}

void CBinTree::searchNode(CTreeNode *node, CTreeNode *&SearchNode, int id) {
    if (node != NULL) {
        if (node->m_Bookid == id) {
            SearchNode = node;
            return;
        } else {
            if (node->l != NULL) {
                searchNode(node->l, SearchNode, id);
            }
            if (node->r != NULL) {
                searchNode(node->r, SearchNode, id);
            }
        }
    }
}


void CBinTree::newTree(CTreeNode *&newroot, CTreeNode *node, CTreeNode *del) {
    if (node != NULL && node != del) {
        CTreeNode *newnode = new CTreeNode(*node);
        addNode(newroot, newnode);
    }

    if (node->l != NULL) {
        newTree(newroot, node->l, del);
    }
    if (node->r != NULL) {
        newTree(newroot, node->r, del);
    }

}

void CBinTree::writeTree(CTreeNode *node, char *file, bool app) {
    node->writeNode(file, app);
    app = true;
    if (node->l != NULL) {
        writeTree(node->l, file, app);
    }
    if (node->r != NULL) {
        writeTree(node->r, file, app);
    }
}


void CBinTree::searchAuthor(CTreeNode *node, char *name, int &n) {
    if (!strcmp(node->m_Author, name)) {
        node->printNode();
        n++;
    }
    if (node->l != NULL) {
        searchAuthor(node->l, name, n);
    }
    if (node->r != NULL) {
        searchAuthor(node->r, name, n);
    }
}


void CBinTree::searchTitle(CTreeNode *node, char *name, int &n) {
    if (!strcmp(node->m_Title, name)) {
        node->printNode();
        n++;
    }
    if (node->l != NULL) {
        searchAuthor(node->l, name, n);
    }
    if (node->r != NULL) {
        searchAuthor(node->r, name, n);
    }
}


void CBinTree::deleteTree(CTreeNode *&node) {
    if (node->l != NULL) {
        deleteTree(node->l);
    }
    if (node->r != NULL) {
        deleteTree(node->r);
    }
    delete node;
    node = NULL;
}

#include <iostream>
#include <fstream>
#include "CTreeNode.h"

CTreeNode::CTreeNode() {
    m_Bookid = 0;
    strcpy(m_Author, "");
    strcpy(m_Title, "");
    m_Year = 0;
    m_Quantity = 0;
}

CTreeNode::CTreeNode(int M_Bookid, char *M_Author, char *M_Title, int M_Year, int M_Quantity) {
    m_Bookid = M_Bookid;
    strcpy(m_Author, M_Author);
    strcpy(m_Title, M_Title);
    m_Year = M_Year;
    m_Quantity = M_Quantity;
}

CTreeNode::CTreeNode(const CTreeNode &node) {
    m_Bookid = node.m_Bookid;
    strcpy(m_Author, node.m_Author);
    strcpy(m_Title, node.m_Title);
    m_Year = node.m_Year;
    m_Quantity = node.m_Quantity;
}

void CTreeNode::printNode() {
    if (this != NULL) {
        std::cout << "\n\n" << this->m_Bookid;
        std::cout << "\n" << this->m_Author;
        std::cout << "\n" << this->m_Title;
        std::cout << "\n" << this->m_Year;
        std::cout << "\n" << this->m_Quantity;
    }

}

void CTreeNode::writeNode(char *file, bool flag) {
    std::ofstream fo;
    if (flag) {
        fo.open(file, std::ios::app);
        fo << "\n" << this->m_Bookid << ";" << this->m_Author << ";" << this->m_Title << ";" << this->m_Year << ";"
           << this->m_Quantity;
    } else {
        fo.open(file);
        fo << this->m_Bookid << ";" << this->m_Author << ";" << this->m_Title << ";" << this->m_Year << ";"
           << this->m_Quantity;
    }


    fo.close();
}

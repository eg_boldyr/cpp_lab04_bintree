#include <iostream>
#include <fstream>

#include <curses.h>
#include "CBinTree.h"

int main() {

    CBinTree *bt = new CBinTree();

    int num;
    while (true) {
        system("cls");
        std::cout << "\n 1 - Info about all books in library";
        std::cout << "\n 2 - Add new books";
        std::cout << "\n 3 - Erase books from library";
        std::cout << "\n 4 - Search books by author";
        std::cout << "\n 5 - Search books by title";
        std::cout << "\n 6 - Exit";
        std::cout << "\n\n Your choice: ";
        std::cin >> num;
        bt->readTree("books.csv");
        switch (num) {
            case 1: {
                bt->readTree("books.csv");
                std::cout << "\nBooks:";
                int nodes = 0;
                bt->printTree(bt->root, nodes);
                if (nodes == 0) {
                    std::cout << "\n\n There are no any books in library";
                }
                getch();
                break;
            }

            case 2: {
                int n, year, quantity, id;
                char author[127], title[127];
                CTreeNode *node;

                std::cout << "\n\nInput count of new books: ";
                std::cin >> n;
                for (int i = 0; i < n; i++) {
                    std::cout << "\nId: ";
                    std::cin >> id;
                    std::cout << "\nAuthor: ";
                    std::cin.sync();
                    std::cin.getline(author, 127);
                    std::cin.sync();
                    std::cout << "\nTitle: ";
                    std::cin.sync();
                    std::cin.getline(title, 127);
                    std::cin.sync();
                    std::cout << "\nYear: ";
                    std::cin >> year;
                    std::cout << "\nQuantity: ";
                    std::cin >> quantity;
                    node = new CTreeNode(id, author, title, year, quantity);
                    node->writeNode("books.csv", true);
                    bt->addNode(bt->root, node);
                }
                n = 0;
                std::cout << "\n\nResult of addition new books:";
                bt->printTree(bt->root, n);
                getch();
                break;
            }

            case 3: {

                CTreeNode *node;
                int n;
                std::cout << "\n\nInput id of Book for erasing ";
                std::cin >> n;
                node = NULL;
                bt->searchNode(bt->root, node, n);
                if (node == NULL) {
                    std::cout << "\n\nThere are no books in library with id " << n;
                    getch();
                } else {
                    std::cout << "\nResult of search node for erasing: ";
                    node->printNode();
                    std::cout << "\n\nDo you want to erase book? (y/n) ";
                    char c;
                    std::cin >> c;
                    if (c == 'y' || c == 'Y') {
                        bt->newTree(bt->newroot, bt->root, node);
                        bt->deleteTree(bt->root);
                        bt->root = bt->newroot;
                        bt->newroot = NULL;

                        if (bt->root != NULL) {
                            std::cout << "\nBook is erased. Available books in library are :";
                            int n = 0;
                            bt->printTree(bt->root, n);
                            bt->writeTree(bt->root, "books.csv", false);
                        } else {
                            std::cout << "\nBook is erased. There are no any available books in library.";
                            std::ofstream fo("books.csv");
                            fo.close();
                        }

                        getch();
                    }
                }

                break;
            }

            case 4: {

                char name[127];
                int n = 0;
                std::cout << "\n\nInput Author of Book for seaching ";
                std::cin.sync();
                std::cin.getline(name, 127);
                std::cin.sync();

                bt->searchAuthor(bt->root, name, n);
                if (n == 0) {
                    std::cout << "\n\nThere are no Books of Author " << name;
                    getch();
                }

                getch();
                break;
            }

            case 5: {

                char name[127];
                int n = 0;
                std::cout << "\n\nInput Title of Book for seaching ";
                std::cin.sync();
                std::cin.getline(name, 127);
                std::cin.sync();

                bt->searchTitle(bt->root, name, n);
                if (n == 0) {
                    std::cout << "\n\nThere are no Books with Title " << name;
                    getch();
                }

                getch();

                break;
            }

            case 6: {
                exit(0);
                break;
            }

            default: {
                std::cout << "\nPlease, enter correct number";
                getch();
                break;
            }

        }
    }
}


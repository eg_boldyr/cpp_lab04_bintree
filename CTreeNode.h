/*!
 * file: CTreeNode.h
 * TreeNode class definition
 */
#pragma once

#include <string.h>
#include <stdio.h>

class CTreeNode {
public:
    //constructors destructor
    CTreeNode();

    CTreeNode(int, char *, char *, int, int);

    CTreeNode(const CTreeNode &);

    ~CTreeNode() {};

    //methods
    void printNode();

    void writeNode(char *, bool);

    //attributes
    int m_Bookid;
    char m_Author[127];
    char m_Title[127];
    int m_Year;
    int m_Quantity;
    CTreeNode *l, *r;
};
